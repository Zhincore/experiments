const CONFIG = {
  supersampling: 1,
  trianglesPerMin: 24,
  textureChange: 10, // triangles per frame
  sizeThreshold: .05,
  animFrames: 150,
  bandWidth: 3,
  colors: ["#199F58", "#E2001A", "#003E90"],
  gutter: 5,
};

/*module.exports = */class Background {
  constructor() {
    this.canvas = document.getElementById("header-background");
    this.ctx = this.canvas.getContext('2d');
    this.animFrame = null;
    this.handlers = {
      resize: this.sizeCanvas.bind(this),
    };
    this.texture = [];
    this.renderData = {};

    this.sizeCanvas();
  }

  sizeCanvas() {
    const { innerWidth, innerHeight } = window;
    this.canvas.width = innerWidth * CONFIG.supersampling;
    this.canvas.height = innerHeight * CONFIG.supersampling;
    const { width, height } = this.canvas;
    const size = Math.min(width, height) / CONFIG.trianglesPerMin / 2;
    const gutter = size * CONFIG.gutter / 100;
    const triHalfHeight = size * Math.sin(60 * Math.PI/180);
    const triCountX = Math.ceil(width / (size + gutter) + 1);
    const triCountY = Math.ceil(height / (triHalfHeight*2 + gutter) + 1);
    this.renderData = {
      width, height,
      size,
      gutter,
      triHalfHeight,
      triCountX,
      triCountY,
      maxScrn: Math.max(width, height),
    };
    this.texture.length = 0;
  }

  stop() {
    if (this.animFrame) window.cancelAnimationFrame(this.animFrame);
    $(window).off("resize", this.handlers.resize);
  }

  start() {
    $(window).on("resize", this.handlers.resize);
    this.texture.length = 0;

    const render = () => {
      const {
        width, height,
        size,
        gutter,
        triHalfHeight,
        triCountX,
        triCountY,
        maxScrn,
      } = this.renderData;

      // Generate texture
      if (this.texture.length == 0) {
        for (let x = 0; x < triCountX; x++) {
          this.texture.push([]);
          for (let y = 0; y < triCountY; y++) {
            this.texture[x].push(
              CONFIG.colors[Math.floor(Math.random() * CONFIG.colors.length)]
            );
          }
        }
      }
      // Randomly change texture
      (async () => {
        for (let i = CONFIG.textureChange; i != 0; i--) {
          const x = Math.floor(Math.random() * this.texture.length);
          this.texture[x][Math.floor(Math.random() * this.texture[x].length)] =
              CONFIG.colors[Math.floor(Math.random() * CONFIG.colors.length)];
        }
      })();

      this.ctx.clearRect(0, 0, width, height);

      let rowDirectionSwitch = false;
      const a = 1 / CONFIG.bandWidth;
      const frame = this.animFrame / CONFIG.animFrames + 1.6;

      for (let x = triCountX - 1; x != 0; x--) {
        const _x = x * (size + gutter) - gutter - size / 2;
        const distX = _x - width / 2;
        let directionSwitch = rowDirectionSwitch = !rowDirectionSwitch;

        for (let y = triCountY - 1; y != 0; y--) {
          const _y = y * (triHalfHeight*2 + gutter) - gutter - size;
          const distY = _y - height / 2;
          const direction = 1 - directionSwitch * 2;
          const value = (frame - Math.sqrt(distX**2 + distY**2) / maxScrn) % 1;
          const scale = Math.max(0, Math.abs((value - .5) * 2) - a) / (1 - a);

          if (scale > CONFIG.sizeThreshold) {
            const _size = size * scale;
            const _triHalfHeight = triHalfHeight * scale;
            this.ctx.fillStyle = this.texture[x][y];

            this.ctx.beginPath();
            this.ctx.moveTo(_x, _y - _triHalfHeight * direction);
            this.ctx.lineTo(_x - _size, _y + _triHalfHeight * direction);
            this.ctx.lineTo(_x + _size, _y + _triHalfHeight * direction);
            this.ctx.lineTo(_x, _y - _triHalfHeight * direction);
            this.ctx.fill();
          }

          directionSwitch = !directionSwitch;
        }
      }

      this.animFrame = window.requestAnimationFrame(render);
    }
    render();
  }
}

new Background().start();