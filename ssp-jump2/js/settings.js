var SETTINGS_KEYS = [
  "_quality",
  "shadows",
  "shadowSize",
  "fxaa",
  "antialias",
  "smoothing",
  "shading",
  "dynLights",
  "supersampling",
  "precision",
  "correctLights",
  "moveCamera",
  "vsync",
  "showFps",
  "visualizePhysics",
  "autoFail"
];

$("#settings-quality").change((ev)=>{

  switch ( $("#settings-quality").val() ){
    case "tetris":
      game.settings.graphics.shadows = false;
      game.settings.graphics.shadowSize = 512;
      game.settings.graphics.fxaa = false;
      game.settings.graphics.antialias = false;
      game.settings.graphics.smoothing = false;
      game.settings.graphics.shading = false;
      game.settings.graphics.dynLights = false;
      game.settings.graphics.correctLights = false;
      game.settings.graphics.supersampling = 0.05;
      game.settings.graphics.vsync = true;
      game.settings.graphics.precision = undefined;
      break;

    case "xlow":
      game.settings.graphics.shadows = false;
      game.settings.graphics.shadowSize = 512;
      game.settings.graphics.fxaa = false;
      game.settings.graphics.antialias = false;
      game.settings.graphics.smoothing = true;
      game.settings.graphics.shading = false;
      game.settings.graphics.dynLights = false;
      game.settings.graphics.correctLights = false;
      game.settings.graphics.supersampling = 0.6;
      game.settings.graphics.vsync = true;
      game.settings.graphics.precision = undefined;
      break;

    case "low":
      game.settings.graphics.shadows = false;
      game.settings.graphics.shadowSize = 512;
      game.settings.graphics.fxaa = false;
      game.settings.graphics.antialias = false;
      game.settings.graphics.smoothing = true;
      game.settings.graphics.shading = true;
      game.settings.graphics.dynLights = false;
      game.settings.graphics.correctLights = false;
      game.settings.graphics.supersampling = 0.9;
      game.settings.graphics.vsync = true;
      game.settings.graphics.precision = undefined;
      break;

    case "normal":
      game.settings.graphics.shadows = true;
      game.settings.graphics.shadowSize = 512;
      game.settings.graphics.fxaa = false;
      game.settings.graphics.antialias = true;
      game.settings.graphics.smoothing = true;
      game.settings.graphics.shading = true;
      game.settings.graphics.dynLights = false;
      game.settings.graphics.correctLights = false;
      game.settings.graphics.supersampling = 1;
      game.settings.graphics.vsync = true;
      break;

    case "high":
      game.settings.graphics.shadows = true;
      game.settings.graphics.shadowSize = 1024;
      game.settings.graphics.fxaa = true;
      game.settings.graphics.antialias = true;
      game.settings.graphics.smoothing = true;
      game.settings.graphics.shading = true;
      game.settings.graphics.dynLights = true;
      game.settings.graphics.correctLights = false;
      game.settings.graphics.supersampling = 1;
      game.settings.graphics.vsync = true;
      game.settings.graphics.precision = undefined;
      break;

    case "custom":
      updateGUISettings(null, false);
      $("#settings-custom").slideDown("fast");
      break;
  }

  if($("#settings-quality").val() != "custom"){
    $("#settings-custom").slideUp("fast");
  }

  saveSettings({
    _quality: $("#settings-quality").val()
  });

});


///// Generate listeners
SETTINGS_KEYS.forEach((key)=>{
  if(key.startsWith("_")) return;

  $("#settings-"+key).change((ev)=>{
    var $target = $(ev.currentTarget);
    var namespace = $target.attr("data-namespace");
    var value;

    if($target.is("input")){
      value = $target.prop("checked");
    }else if($target.is("select")){
      value = $target.val();
      value = isNaN(value) ? value : Number(value);
    }

    if(namespace){
      game.settings[namespace][key] = decodeValue(value);
    }else{
      game.settings[key] = decodeValue(value);
    }

    var data = {};
    data[key] = value;

    saveSettings(data);
  });
});


$("#settings-shadows").change(()=>{
  if($("#settings-shadows").prop("checked")) {
    $("#settings-shadowSize-wrap").slideDown("fast");
  } else $("#settings-shadowSize-wrap").slideUp("fast");
});

// $("#settings-shadowSize").change(()=>{
//   game.settings.graphics.shadowSize = Number($("#settings-shadowSize").val());
//
//   saveSettings({shadowSize: game.settings.graphics.shadowSize});
// });
// $("#settings-fxaa").change(()=>{
//   game.settings.graphics.fxaa = $("#settings-fxaa").prop("checked");
//
//   saveSettings({fxaa: game.settings.graphics.fxaa});
// });
// $("#settings-shading").change(()=>{
//   game.settings.graphics.shading = $("#settings-shading").prop("checked");
//
//   saveSettings({shading: game.settings.graphics.shading});
// });
// $("#settings-dynLights").change(()=>{
//   game.settings.graphics.dynLights = $("#settings-dynLights").prop("checked");
//
//   saveSettings({dynLights: game.settings.graphics.dynLights});
// });
// $("#settings-supersampling").change(()=>{
//   game.settings.graphics.supersampling = Number($("#settings-supersampling").val());
//
//   saveSettings({supersampling: game.settings.graphics.supersampling});
// });
// $("#settings-vsync").change(()=>{
//   game.settings.graphics.vsync = Number($("#settings-vsync").val());
//
//   saveSettings({vsync: game.settings.graphics.vsync});
// });





// function updateBackendSettings(settings) {
//   SETTINGS_KEYS.forEach((key)=>{
//     if(key.startsWith("_")) return;
//     if((key in settings) && (key in game.settings.graphics)){
//       game.settings.graphics[key] = settings[key];
//     }
//   });
//
//   if("_quality" in settings){
//     $("#settings-quality").val(settings._quality).trigger("change");
//   }
//
//   updateGUISettings();
//
//   return;
// }

function decodeValue(value){
  if(value == "undefined") value = undefined;
  return value;
}

function encodeValue(value){
  if(value === undefined) value = "undefined";
  return value;
}


function updateGUISettings(settings, triggerUpdate=true){
  SETTINGS_KEYS.forEach((key)=>{
    if(key.startsWith("_")) return;

    var $target = $("#settings-"+key);
    var namespace = $target.attr("data-namespace");
    var value;

    if(settings && key in settings){
      value = settings[key];

    }else{
      if(namespace){
        value = encodeValue(game.settings[namespace][key]);
      }else{
        value = encodeValue(game.settings[key]);
      }
    }

    if($target.is("input")){
      $target.prop("checked", value);

    }else if($target.is("select")){
      $target.val(value);
    }

    if(triggerUpdate) (async ()=>{$target.trigger("change");})();
  });

  if(settings && "_quality" in settings){
    $("#settings-quality").val(settings._quality);
    if(triggerUpdate) (async ()=>{$("#settings-quality").trigger("change");})();
  }

  return;

  // $("#settings-shadows").prop("checked", game.settings.graphics.shadows);
  // if(game.settings.graphics.shadows) {
  //   $("#settings-shadowSize-wrap").slideDown("fast");
  // } else $("#settings-shadowSize-wrap").slideUp("fast");
  // $("#settings-shadowSize").val(String(game.settings.graphics.shadowSize));
  // $("#settings-fxaa").prop("checked",game.settings.graphics.fxaa);
  // $("#settings-shading").prop("checked",game.settings.graphics.shading);
  // $("#settings-dynLights").prop("checked",game.settings.graphics.dynLights);
  // $("#settings-supersampling").val(String(game.settings.graphics.supersampling));
  // $("#settings-vsync").prop("checked",game.settings.graphics.vsync);
}
