$(".custom-select").click((ev)=>{
  ev.preventDefault();
  ev.stopPropagation();
  var $select = $("select", ev.currentTarget);
  var options = $select.children("option");
  var activeI = $select.prop("selectedIndex");
  var destroing = false;


  /// Create chooser
  var $wrap = $(`<div class="gui overlay overlay-bg gui-menu text-white flex-center"></div>`);

  var $chooser = $(`<ul class="custom-select-chooser"></ul>`);
  $wrap.append($chooser);

  var destroy = ()=>{
    $wrap.fadeOut("fast", ()=>{
      $wrap.remove();
    });
  }

  $wrap.click(destroy);


  /// Add options
  options.each((i, option)=>{
    var $option = $(option);

    $item = $(`
      <li class="${activeI == i ? "active": ""}" data-value="${$option.val()}">
        ${$option.text()}
      </li>
    `);
    $item.click((ev)=>{
      if(destroing) return;
      destroing = true;
      ev.stopPropagation();
      var $target = $(ev.currentTarget);

      $chooser.children("li").removeClass("active");
      $target.addClass("active");
      $select.val($target.attr("data-value")).trigger("change");

      setTimeout(destroy, 100);

      return false;
    });
    $chooser.append($item);
  });


  /// Show chooser
  $(document.body).append($wrap.hide().fadeIn("fast"));



  return false;
});
