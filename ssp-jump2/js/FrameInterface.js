var FrameInterface = {
  key: null,

  isAuth: function () {
    var key = "af21b65a810c84f90b5e77a08b1ffa0590b3291af17064855c32e31d3f996934";

    return key === this.key;
  },

  init: function () {
    ///// Register events
    $(window).on("message", (ev)=>this.processMessage(ev.originalEvent.data));
  },

  //// Zhincore Insecure Interface Protocol
  processMessage: async function (data) {
    var protocol = "ZIIP";
    var settable = {
      "game": [],
      "renderer": ["renderer"],
      "enviroment": ["enviroment"],
      "player": ["player"]
    };
    if(typeof data !== 'string' || !data.startsWith(protocol+"://")) return true;
    data = data.replace(new RegExp(`^${protocol}\:\/\/`), "").replace(/\/$/, "");
    var args = data.split("/");
    var helper0 = args.shift().split(":");
    var cmd = helper0.shift();
    var pass = helper0.shift();

    function useTarget(obj, path, set=false, call=false, value=null) {
      var target = obj;
      var lastValue;
      path.forEach((item, i)=>{
        if(i == path.length-1){
          if(set){
            lastValue = target[item];
            target[item] = value;
          }else if(call){
            lastValue = target[item]();
          }
        }

        target = target[item];
      });
      return set ? lastValue : target;
    }

    if(cmd == "auth"){
      this.key = await sha256(pass);
      if(this.isAuth()) console.info("ZhincoreInsecureInterfaceProtocol ready!");

    }else if(this.isAuth()){
      if(cmd == "set" || cmd == "get" || cmd == "call"){
        if(!(args[0] in settable)) return true;

        var domainPath = args.shift();
        var value  = isNaN(pass) ? (pass === "false" ? false : (pass === "true" ? true : pass)) : Number(pass);
        var path = [];

        if(settable[domainPath]){
          path = path.concat(settable[domainPath]);
        }

        path = path.concat(args);

        var lastValue = useTarget(game, path, cmd == "set", cmd == "call", value);
        if(cmd == "call"){
          console.log("Success. Result is:", lastValue);
        }else if(cmd == "set"){
          console.log("Success. Changed", lastValue, "to", value);
        }else{
          console.log("Success. Value is:", lastValue);
        }
      }

    }

    return true;
  }

}
