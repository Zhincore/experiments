class Player{
  constructor(settings={}, game){
    this.game = game;
    this.settings = Object.assign({
      playerModel: "rubberduck",
      jumpForce: 1.25,
      moveForce: 1
    }, settings);

    this.obj = null;
    this.canJump = true;
    this.shouldJump = false;

    this.momentum = new THREE.Vector3();

    this.hitboxHelpers = [];
  }


  ///// Movement
  jump(){
    if(this.canJump) {
      this.shouldJump = true;
      this.canJump = false;
    }
  }

  left(){
    this.momentum.add(new THREE.Vector3(this.settings.moveForce, 0, 0));
  }
  right(){
    this.momentum.add(new THREE.Vector3(-this.settings.moveForce, 0, 0));
  }


  ///// Reset
  reset(){
    this.obj.position.set(0, 0, 0);
    this.canJump = true;
    this.shouldJump = false;
    this.momentum = new THREE.Vector3();
  }


  ///// Load
  async load(progress=()=>{}){
    this.obj = (await this.game.loadModel(this.settings.playerModel, progress)).scene.children[0];
    this.obj.traverse((obj)=>{ obj.castShadow = true; });
    this.obj.geometry.computeBoundingBox();
    progress(100);
  }

  ///// Update
  update(deltaFactor){
    var position = this.obj.position;
    var momentum = this.momentum;
    if(this.shouldJump) {
      momentum.y += this.settings.jumpForce;
      this.shouldJump = false;
    }

    // Push player to a wanted position
    momentum.x += (Math.round(position.x) - position.x) * deltaFactor / 6;


    // Apply force
    var nowmentum = momentum.clone().multiplyScalar(0.21 * deltaFactor);
    position.add( nowmentum );
    momentum.sub( nowmentum );


    var groundCollide = (y=0)=>{
      position.y = y;
      momentum.y = 0;
      this.canJump = true;
    };

    //// Collisions
    this.obj.updateMatrixWorld();
    var pbox = this.obj.geometry.boundingBox.clone();
    pbox.applyMatrix4(this.obj.matrixWorld);
    pbox.expandByScalar(-0.1);
    this.hitboxHelpers.forEach((obj)=>{
      this.game.scene.remove(obj);
    });
    if(this.game.settings.visualizePhysics){
      this.hitboxHelpers = [new THREE.Box3Helper( pbox )];
      this.game.scene.add(this.hitboxHelpers[0]);
    }
    /// obstacles
    this.game.enviroment.getActiveSegments().some((segment, i)=>{
      var collided = false;

      segment.obstacles.forEach((obstacle)=>{
        //obstacle.updateMatrixWorld();
        var box2 = obstacle.geometry.boundingBox.clone();
        box2.applyMatrix4(obstacle.matrixWorld);

        if(this.game.settings.visualizePhysics){
          var helper = new THREE.Box3Helper( box2, 0x00ffff );
          this.game.scene.add(helper);
          this.hitboxHelpers.push(helper);
        }

        var intersection = pbox.clone().intersect(box2);

        if(!intersection.isEmpty()){
          var collision = intersection.getCenter(new THREE.Vector3());
          var hitbox    = box2.getSize(new THREE.Vector3());
          var hitboxPos = box2.getCenter(new THREE.Vector3());
          //var playerbox = box1.getSize(new THREE.Vector3());
          var vector    = (intersection.getCenter(new THREE.Vector3()).sub(this.obj.position));

          if(position.y >= hitboxPos.y){
            groundCollide((hitboxPos.y+hitbox.y/2)*.9);

          }else if(Math.abs(vector.x) > Math.abs(vector.z)){
            position.x -= vector.x;

          }else{
            this.game.end();
          }
          collided = true;
        }
      });

      return collided;
    });
    /// ground
    if(position.y < 0) {
      groundCollide();
    }
    var wallLimit = ENV_WALL_LIMIT;
    /// left wall
    if(position.x > ENV_WALL_LIMIT){
      position.x = ENV_WALL_LIMIT;
      momentum.x = 0;
    /// right wall
    }else if(position.x < -ENV_WALL_LIMIT){
      position.x = -ENV_WALL_LIMIT;
      momentum.x = 0;
    }

    // // Apply friction
    // momentum.x *= 0.9;
    // momentum.z *= 0.9;
    // Apply gravity
    momentum.y -= 9.8 * deltaFactor / 200;
    // // Apply drag
    //momentum.multiplyScalar(delta*58);
  }
}
