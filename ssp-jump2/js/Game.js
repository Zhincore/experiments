class Game{
  constructor(settings={}, onEnd=()=>{}, onStats=()=>{}, onSetHeaderMsg=()=>{}){
    // Set settings with defaults
    this.settings = Object.assign({
      physicsDelta: 1/60,
      maxPhsysicsLoops: 5,
      graphics: {
        shadows: false,
        shadowSize: 512,
        fxaa: false,
        antialias: false,
        smoothing: true,
        shading: true,
        dynLights: false,
        supersampling: 1,
        vsync: true,
        precision: undefined,
        correctLights: false
      },
      showFps: false,
      visualizePhysics: false,
      autoFail: true,
      moveCamera: false,
    }, settings);

    this.onEnd = onEnd;
    this.onStats = onStats;
    this.onSetHeaderMsg = onSetHeaderMsg;


    ///// Init internals
    this.animframe = false;
    this.shouldRun = true;
    this.data = {};
    this.pointers = {};
    this.timing = {
      accumulator: 0
    };
    this.speed = 0;
    this.difficulty = 0;
    this.controls = {
        jump: false,
        left: false,
        right: false
    }


    ///// Prepare THREEJS
    this.gLoader = new THREE.GLTFLoader();
    var dracoLoader = new THREE.DRACOLoader();
    dracoLoader.setDecoderPath(LIB_PATH+'modules/draco/');
    this.gLoader.setDRACOLoader( dracoLoader );
    var ddsLoader = new THREE.DDSLoader();
    this.gLoader.setDDSLoader( ddsLoader );


    ///// Check for WebGL
    if(THREE.WEBGL.isWebGL2Available()){
      this.webglVersion = 2;

    }else if(THREE.WEBGL.isWebGLAvailable()){
      this.webglVersion = 1;

    }else{
      this.onSetHeaderMsg(THREE.WEBGL.getWebGLErrorMessage());
      this.webglVersion = 0;
      throw "WebGL is not available";
    }


    ///// Init stats
    this.stats = new Stats();
    this.stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
    $("#game").append(
      $(this.stats.dom)
        .attr("id", "fps-counter")
        .css("z-index", "unset")
    );


    ///// Create enviroment
    this.enviroment = new Enviroment({}, this);

    ///// Create player
    this.player = new Player({}, this);
  }

  ///////////////
  ///// Helper functions
  ///////////////
  error(msg){
    console.error(msg);
  }

  ///////////////
  ///// INIT
  ///////////////
  init(){
    ///// Init THREEJS
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0x14a25f);
    this.camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.1, 1000 );
    this.camera.position.set(0, 4.5, -5);
    this.camera.rotation.set(0.6, Math.PI, 0);

    ///// Prepare WebGL
    this.webglOptions = {
      failIfMajorPerformanceCaveat: true,
      powerPreference: "high-performance",
      antialias: this.settings.graphics.antialias,
      precision: this.settings.graphics.precision
    };

    this.canvas = document.createElement( 'canvas' );
    if (this.webglVersion == 2){
      this.context = this.canvas.getContext('webgl2', this.webglOptions);
    }else {
      this.context = this.canvas.getContext('webgl', this.webglOptions);
    }

    ///// Init WebGL
    this.renderer = new THREE.WebGLRenderer(Object.assign({}, this.webglOptions, {
      canvas: this.canvas, context: this.context
    }));
    this.renderer.physicallyCorrectLights = this.settings.graphics.correctLights;
    this.renderer.shadowMap.enabled = this.settings.graphics.shadows;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    this.renderer.outputEncoding = THREE.sRGBEncoding;
    $("#game").append( this.renderer.domElement );

    this.clock = new THREE.Clock();


    ///// Init shaders
    this.composer = new THREE.EffectComposer( this.renderer );
    // Scene render
    this.renderPass = new THREE.RenderPass( this.scene, this.camera );

    // FXAA
    this.fxaaPass = new THREE.ShaderPass( THREE.FXAAShader );


    ///// Init enviroment
    this.enviroment.init();


    ///// Init scene
    this.scene.add(this.enviroment.obj);
    if(this.settings.moveCamera) this.player.obj.add(this.camera);
    this.scene.add(this.player.obj);


    ///// Trigger re-sizing
    this.resize();


    ///// Register event listeners
    $(window).resize(()=>{
      this.resize();
    });
    $(this.renderer.domElement).swipe({
      swipeUp:    () => { this.controls.jump = true; },
      swipeLeft:  () => { this.controls.left = true; },
      swipeRight: () => { this.controls.right = true; },
      threshold: 25,
    });
    $(document).keydown((ev)=>{
      switch (ev.code){
        case "ArrowUp":
        case "Space":
        case "KeyW":
          this.controls.jump = true;
          break;
        case "ArrowLeft":
        case "KeyA":
          this.controls.left = true;
          break;
        case "ArrowRight":
        case "KeyD":
          this.controls.right = true;
          break;
      }
    });
  }

  ///////////////
  ///// UPDATE SETTINGS
  ///////////////
  updateSettings(){
    ///// Remove previous settings-related objects
    // DirectionalLight
    if("directionalLight" in this.pointers) {
      this.scene.remove(this.pointers.directionalLight);
      delete this.pointers.directionalLight;
    }
    // AmbientLight
    if("ambientLight" in this.pointers) {
      this.scene.remove(this.pointers.ambientLight);
      delete this.pointers.ambientLight;
    }

    // Composer
    this.composer.reset();
    this.composer.addPass( this.renderPass );
    if(this.settings.graphics.fxaa){
      this.composer.addPass( this.fxaaPass );
    }


    ///// Add settings-related objects
    // DirectionalLight
    if(!this.settings.graphics.dynLights && this.settings.graphics.shading){
      var directionalLight = new THREE.DirectionalLight(0xFaFeFf, .4);
      directionalLight.castShadow = this.settings.graphics.shadows;
      directionalLight.shadow.radius = 5;
      directionalLight.shadow.mapSize.height = this.settings.graphics.shadowSize;
      directionalLight.shadow.mapSize.width = this.settings.graphics.shadowSize;
      this.scene.add( directionalLight );
      this.pointers.directionalLight = directionalLight;
    }
    // AmbientLight
    var ambientLight = new THREE.AmbientLight(0xFFFFFF, 1);
    this.scene.add( ambientLight );
    ambientLight.intensity = (
      this.settings.graphics.shading ?
      0.25 :
      1
    );
    this.pointers.ambientLight = ambientLight;


    ///// Run Enviroment reload sequence
    this.enviroment.reset();

    //// Update renderer
    this.renderer.failIfMajorPerformanceCaveat = this.settings.autoFail;
    this.renderer.antialias = this.settings.graphics.antialias;
    this.renderer.shadowMap.enabled = this.settings.graphics.shadows;

    //// Update CSS
    $(this.renderer.domElement).toggleClass("no-smoothing", !this.settings.graphics.smoothing);

    //// Update FPS counter
    $(this.stats.domElement).toggle(this.settings.showFps);

    ///// Update resolution
    this.resize();

    return;
  }

  ///////////////
  ///// EVENT LISTENERS
  ///////////////
  resize(){
    var width = $(this.canvas).width();
    var height = $(this.canvas).height();

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize( width * this.settings.graphics.supersampling, height * this.settings.graphics.supersampling, false );
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.composer.setSize( width * this.settings.graphics.supersampling, height * this.settings.graphics.supersampling );

    var pixelRatio = this.renderer.getPixelRatio();
		this.fxaaPass.material.uniforms[ 'resolution' ].value.x = 1 / ( width * pixelRatio );
		this.fxaaPass.material.uniforms[ 'resolution' ].value.y = 1 / ( height * pixelRatio );
  }

  ///////////////
  ///// LOAD ASSETS
  ///////////////
  load(progress=()=>{}){
    return new Promise(async (resolve)=>{
      this.data.obstacles = await this.loadJSON("obstacles");
      progress(33);

      await this.player.load((percent)=>{
        progress(33 + (percent / 100) * 32);
      });
      await this.enviroment.load((percent)=>{
        progress(66 + (percent / 100) * 32);
      });

      progress(100);
      return resolve();
    });
  }

  loadModel(model, progress=()=>{}){
    return new Promise((resolve)=>{
      this.gLoader.load(
        // resource URL
        MODEL_PATH+model+"/"+model+"."+MODEL_FORMAT,
        // called when the resource is loaded
        ( gltf )=> {
          resolve(gltf);
        },
        // called while loading is progressing
        ( xhr )=>{
          progress( xhr.loaded / xhr.total * 100 );
        },
        // called when loading has errors
        ( error )=>{
          this.error(error);
        }
      );
    });
  }

  loadJSON(file){
    return new Promise((resolve)=>{
      $.get(DATA_PATH+file+".json", resolve, "json");
    });
  }


  ///////////////
  ///// RENDERLOOP + start
  ///////////////
  start(){
    this.clock.getDelta();

    var render = ()=>{
      this.stats.begin();
      var delta = this.clock.getDelta() || this.settings.physicsDelta;

      /// Update controls
      if(this.controls.jump) {
        this.player.jump();
        this.controls.jump = false;
      }
      if(this.controls.left) {
        this.player.left();
        this.controls.left = false;
      }else
      if(this.controls.right) {
        this.player.right();
        this.controls.right = false;
      }

      /// Logic
      // divides logic into multiple parts if rendering took too long
      var accumulator = delta;
      var physI = 0;
      while(accumulator > 0 && physI < this.settings.maxPhsysicsLoops){
        ///// LOGIC BLOCK
        var physicsDelta = accumulator;
        if(accumulator > this.settings.physicsDelta) {
          physicsDelta = this.settings.physicsDelta;
        }

        var deltaFactor = physicsDelta/this.settings.physicsDelta;

        // Update
        this.enviroment.update(deltaFactor);
        this.player.update(deltaFactor);

        ///// /LOGIC BLOCK
        accumulator -= physicsDelta;
        physI++;
      }

      // Render
      this.composer.render( this.scene, this.camera );
      //this.renderer.render( this.scene, this.camera );

      this.stats.end();

      (async()=>{
        this.onStats(
          -this.enviroment.obj.position.z,
          this.enviroment.movementSpeed/this.settings.physicsDelta,
          this.enviroment.difficulty
        );
      })();

      // Request next frame
      if(this.shouldRun){
        if(this.settings.graphics.vsync){
          this.animframe = window.requestAnimationFrame(render);
        }else{
          setTimeout(async ()=>{
            render();
          }, 0);
        }

      }
    };

    return render();
  }

  run(){
    this.enviroment.run();
  }

  ///////////////
  ///// COMPILE SHADERS
  ///////////////
  compileShaders(){
    this.updateSettings();
    this.renderer.compile(this.scene, this.camera);
  }

  ///////////////
  ///// STOP RENDERLOOP
  ///////////////
  stop(){
    this.shouldRun = false;
    if(this.animframe) window.cancelAnimationFrame(this.animframe);
    this.animframe = false;
  }

  ///////////////
  ///// RESET
  ///////////////
  reset(){
    this.stop();
    this.renderer.clear();
    this.enviroment.reset();
    this.player.reset();
    this.shouldRun = true;
  }

  ///////////////
  ///// END (player failed)
  ///////////////
  end(){
    //var distance = -this.enviroment.obj.position.z;
    this.stop();
    this.onEnd();
  }
}
