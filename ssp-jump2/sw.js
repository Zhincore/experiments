/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

// Names of the two caches used in this version of the service worker.
// Change to v2, etc. when you update any of the local resources, which will
// in turn trigger the install event again.
const PRECACHE = 'precache-v1';
const RUNTIME = 'runtime';

// A list of local resources we always want to be cached.
const PRECACHE_URLS = [
  'index.html',
  './', // Alias for index.html
  ...(
    `./textures/wall_kachla_bumpmap.png
    ./textures/linoleum.png
    ./textures/wall_kachla.png
    ./css/style.css
    ./css/all.min.css
    ./models/ssp-hall-old_locker/ssp-hall-old_locker.gltf
    ./models/ssp-hall-old_locker/ssp-hall-old_locker.bin
    ./models/ssp-hall-door/ssp-hall-door.bin
    ./models/ssp-hall-door/ssp-hall-door.png
    ./models/ssp-hall-door/ssp-hall-door.gltf
    ./models/rubberduck/rubberduck.gltf
    ./models/rubberduck/rubberduck.png
    ./models/rubberduck/rubberduck.bin
    ./models/cartoon-box/cartoon-box.bin
    ./models/cartoon-box/cartoon-box.gltf
    ./models/ssp-hall-locker-laying/ssp-hall-locker-laying.bin
    ./models/ssp-hall-locker-laying/ssp-hall-locker-laying.gltf
    ./models/ssp-hall-bench/ssp-hall-bench.bin
    ./models/ssp-hall-bench/ssp-hall-bench.gltf
    ./models/ssp-hall-locker/ssp-hall-locker.gltf
    ./models/ssp-hall-locker/ssp-hall-locker.bin
    ./models/lowpoly-backpack/lowpoly-backpack.bin
    ./models/lowpoly-backpack/lowpoly-backpack.gltf
    ./js/settings.js
    ./js/util.js
    ./js/custom-select.js
    ./js/Game.js
    ./js/FrameInterface.js
    ./js/Enviroment.js
    ./js/Player.js
    ./js/lib/modules/ShaderPass.js
    ./js/lib/modules/EffectComposer.js
    ./js/lib/modules/FXAAShader.js
    ./js/lib/modules/CopyShader.js
    ./js/lib/modules/DRACOLoader.js
    ./js/lib/modules/DDSLoader.js
    ./js/lib/modules/GLTFLoader.js
    ./js/lib/modules/RenderPass.js
    ./js/lib/modules/draco/draco_wasm_wrapper.js
    ./js/lib/modules/draco/draco_encoder.js
    ./js/lib/modules/draco/README.md
    ./js/lib/modules/draco/draco_decoder.wasm
    ./js/lib/modules/draco/gltf/draco_wasm_wrapper.js
    ./js/lib/modules/draco/gltf/draco_encoder.js
    ./js/lib/modules/draco/gltf/draco_decoder.wasm
    ./js/lib/modules/draco/gltf/draco_decoder.js
    ./js/lib/modules/draco/draco_decoder.js
    ./js/lib/jquery.touchSwipe.min.js
    ./js/lib/three.min.js
    ./js/lib/stats.min.js
    ./js/lib/jquery-3.4.1.min.js
    ./js/lib/jquery-3.4.1.min.map
    ./js/lib/ColorifyShader.js
    ./js/lib/WebGL.js
    ./data/obstacles.json
    ./index.html
    ./webfonts/fa-brands-400.woff2
    ./webfonts/fa-solid-900.ttf
    ./webfonts/fa-regular-400.woff
    ./webfonts/fa-brands-400.ttf
    ./webfonts/fa-brands-400.woff
    ./webfonts/fa-solid-900.svg
    ./webfonts/fa-solid-900.woff
    ./webfonts/fa-regular-400.ttf
    ./webfonts/fa-solid-900.eot
    ./webfonts/NotoColorEmoji.ttf
    ./webfonts/fa-regular-400.eot
    ./webfonts/fa-brands-400.svg
    ./webfonts/fa-regular-400.svg
    ./webfonts/fa-brands-400.eot
    ./webfonts/fa-regular-400.woff2
    ./webfonts/fa-solid-900.woff2
  `).split("\n").map(v => v.trim()),
];

// The install handler takes care of precaching the resources we always need.
self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(PRECACHE)
      .then(cache => cache.addAll(PRECACHE_URLS))
      .then(self.skipWaiting())
  );
});

// The activate handler takes care of cleaning up old caches.
self.addEventListener('activate', event => {
  const currentCaches = [PRECACHE, RUNTIME];
  event.waitUntil(
    caches.keys().then(cacheNames => {
      return cacheNames.filter(cacheName => !currentCaches.includes(cacheName));
    }).then(cachesToDelete => {
      return Promise.all(cachesToDelete.map(cacheToDelete => {
        return caches.delete(cacheToDelete);
      }));
    }).then(() => self.clients.claim())
  );
});

// The fetch handler serves responses for same-origin resources from a cache.
// If no response is found, it populates the runtime cache with the response
// from the network before returning it to the page.
self.addEventListener('fetch', event => {
  // Skip cross-origin requests, like those for Google Analytics.
  if (event.request.url.startsWith(self.location.origin)) {
    event.respondWith(
      caches.match(event.request).then(cachedResponse => {
        if (cachedResponse) {
          return cachedResponse;
        }

        return caches.open(RUNTIME).then(cache => {
          return fetch(event.request).then(response => {
            // Put a copy of the response in the runtime cache.
            return cache.put(event.request, response.clone()).then(() => {
              return response;
            });
          });
        });
      })
    );
  }
});
