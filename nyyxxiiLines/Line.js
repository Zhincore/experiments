class Line {
  constructor(options, x=0, y=0, angle=0) {
    this.options = options;
    this.backlog = Array(options.backlog).fill(0).map(_ => [x, y]);
    this.angle = angle;
    this.hueShift = Math.random();
    this.segmentSize = options.speed * options.size;
  }

  move(bend=0) {
    const pos = this.backlog[this.backlog.length - 1];
    const newPos = [this.segmentSize, 0];

    rotateVector(newPos, this.angle += bend);
    newPos[0] = pos[0] + Math.round(newPos[0]);
    newPos[1] = pos[1] + Math.round(newPos[1]);

    this.backlog.shift();
    this.backlog.push(newPos);
  }

  shiftHue(shift) {
    this.hueShift = Math.max(0, Math.min(1, this.hueShift + shift));
  }

  render(ctx) {
    const { colorBase, colorRange, size, speed, bloom, segment } = this.options;
    const hue = colorBase + this.hueShift * colorRange;

    ctx.strokeStyle = `hsl(${hue}deg, 100%, 50%)`;
    ctx.shadowColor =  `hsl(${hue}deg, 100%, 60%, ${bloom.intensity * 100}%)`;
    ctx.shadowBlur = bloom.size;
    ctx.lineWidth = size;

    // Clamp posision
    const pos = this.backlog[this.backlog.length - 1];
    const clampedPos = [
      (ctx.canvas.width + pos[0]) % ctx.canvas.width,
      (ctx.canvas.height + pos[1]) % ctx.canvas.height,
    ];

    if (pos[0] != clampedPos[0] || pos[1] != clampedPos[1]) {
      // this.backlog.forEach(p => (p[0] = clampedPos[0]) && (p[1] = clampedPos[1]));
      pos[0] = clampedPos[0];
      pos[1] = clampedPos[1];
    }

    const backlog = [...this.backlog];
    const pointBuffer = backlog.splice(0, segment);

    for (let i = 0; i < backlog.length; i++) {
      let teleported = false;
      let lastPoint = pointBuffer.shift();

      ctx.beginPath();
      ctx.moveTo(...lastPoint);
      for (const point of pointBuffer) {
        ctx.lineTo(...point);

        if (
          Math.abs(point[0] - lastPoint[0]) >= (ctx.canvas.width - this.segmentSize) ||
          Math.abs(point[1] - lastPoint[1]) >= (ctx.canvas.height - this.segmentSize) 
        ) teleported = true;

        lastPoint = point;
      }
      pointBuffer.push(backlog[i]);
      if (teleported) continue;

      ctx.globalAlpha = (i / backlog.length) ** 2;
      ctx.stroke();
    }
    ctx.globalAlpha = 1;
  }
}


function rotateVector(vec, ang) {
  ang = -ang * (Math.PI/180);
  const cos = Math.cos(ang);
  const sin = Math.sin(ang);
  const newVec = [
    vec[0] * cos - vec[1] * sin,
    vec[0] * sin + vec[1] * cos
  ];
  vec[0] = newVec[0];
  vec[1] = newVec[1];
  return vec;
}
