const canvas = document.getElementsByTagName("canvas")[0];
const ctx = canvas.getContext("2d");

const number = 5;
const zigzagness = 0.1;
const colorChange = 0.1;
const options = {
  size: 4,
  speed: 1,
  bloom: {
    size: 0,
    intensity: .5,
  },
  colorRange: 45,
  colorBase: 260,
  backlog: 128,
  segment: 5,
};

let frame, lines = null;

function resize(){
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;
}

function render(){
  frame = window.requestAnimationFrame(render);

  ctx.clearRect(0, 0, canvas.width, canvas.height);

  ctx.lineCap = "round";
  // ctx.globalCompositeOperation = "lighter";

  for (const line of lines) {
    let bend = 0;
    if (Math.random() < (zigzagness * options.speed) / options.size) {
      bend = Math.sign(Math.random() - 0.5) * 45;
    }
    line.move(bend);
    line.shiftHue((Math.random() - 0.5) * colorChange);

    line.render(ctx);
  }
}


resize();
window.addEventListener("resize", resize);

lines = Array(number).fill(0).map(_ => new Line(
  options,
  canvas.width / 2,
  canvas.height / 2,
  Math.floor(Math.random() * 8) / 8 * 360
));

render();
