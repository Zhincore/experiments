class NeuralNetwork{
  constructor(layers=[8], nInputs=8, nOutputs=1, network){
    this.nInputs = nInputs;
    this.layers = [];
    if(network){
      this.import(network);
    }else{
      this.import(JSON.stringify({
        nInputs: 8,
        layers: [
          [
            {
              bias: 0,
              weights: [1,-0.5,0.5,-0.5,0.5,-0.7,-0.25,-0.25]
            },
            {
              bias: 0,
              weights: [-0.5,1,0.5,-0.5,-0.5,0.25,-1,0.5]
            },
            {
              bias: 0,
              weights: [-0.5,-0.5,-0.5,1,0.5,0.25,0.5,-1]
            }
          ]
        ]
      }));
      // for(const nNodes of [...layers, nOutputs]){
      //   const layerI = this.layers.push([]) -1;
      //   for(let i = 0; i < nNodes; i++){
      //     this.layers[layerI].push({
      //       bias: 0,
      //       weights: ((layerI == 0
      //         ? Array(nInputs)
      //         : Array(this.layers[layerI-1].length)
      //       ).fill(0).map(()=>0))
      //     });
      //   }
      // }
      this.mutate(0.5); // randomise
    }
  }

  export(){
    return JSON.stringify({
      nInputs: this.nInputs,
      layers: this.layers
    });
  }
  import(data){
    data = JSON.parse(data);
    this.nInputs = data.nInputs;
    this.layers = data.layers;
  }
  clone(network){
    this.import(network.export());
  }

  /**
   * mutate - description
   *
   * @param  {type} rate=0.1 description
   * @return {type}          description
   */
  mutate(rate=0.1){
    function mutate(value, min=-1, max=1) {
      // return Math.max(min, Math.min(max, value + Math.random() * rate - rate / 2));
      return Math.max(min, Math.min(max, value + (0.5 - Math.random()) * 2 * rate))
    }

    for(const layer of this.layers){
      for(const node of layer){
        node.bias = mutate(node.bias, 0);
        node.weights = node.weights.map(v=>mutate(v));
      }
    }
  }

  async evaluate(input){
    let activations = [];

    for(const layer of this.layers){
      const i = activations.push([]) - 1;
      const prevLayer = (i == 0 ? input : activations[i-1]);
      for(const node of layer){
        let activation = 0;
        for(let j = 0; j < prevLayer.length; j++){
          activation += prevLayer[j] * node.weights[j];
        }
        activations[i].push(Math.max(0, sigmoid(activation) - node.bias));
      }
    }

    return {
      activations,
      result: activations[activations.length-1]
    };
  }
}

function sigmoid(t) {
  return 1/(1+Math.pow(Math.E, -t));
}
