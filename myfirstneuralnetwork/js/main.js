Number.prototype.mod = function (n) {
  return ((this % n) + n) % n;
};

(() => {
  let shouldTrain = true;
  let layers = [];
  let batchSize = 64;
  let inheritRate = 0.7;
  let mutationRate = 0.02;
  let delay = 10;
  let gameSize = 16;
  const $nnDisplay = $("#nn-render");

  let workers = [];
  let generation = [];
  let generationI = 0;
  let training = false;

  const displayGame = new Snake("#rightpane", gameSize);
  displayGame.render();

  $(document).keydown((ev) => {
    if (!training && ev.code.toLowerCase().includes("arrow")) {
      ev.preventDefault();
      displayGame.move(ev.code.toLowerCase().replace("arrow", ""));
      renderFakeNetwork();
    }
  });
  $(window).resize(() => {
    displayGame.render();
  });

  $(".controlButton").click(function () {
    switch ($(this).attr("data-cmd")) {
      case "train":
        if (!training) train();
        training = true;
        break;
      case "stoptrain":
        shouldTrain = false;
        break;
    }
  });

  /**
   * renderFakeNetwork - description
   *
   * @return {type}  description
   */
  function renderFakeNetwork() {
    const input = getInput(displayGame.game);
    if (!$("#fakeNetwork", $nnDisplay).length) {
      $nnDisplay.html(
        $("<div></div>")
          .attr("id", "fakeNetwork")
          .html(
            renderNeuralNetwork(
              { layers: [0] },
              {
                width: $nnDisplay.width(),
                height: $nnDisplay.height(),
                inputs: Object.keys(input),
              },
            ),
          ),
      );
    }

    updateRenderedNetwork(input, [[]]);
  }

  /**
   * train - description
   *
   * @return {type}  description
   */
  async function train() {
    training = true;
    shouldTrain = true;
    let lastMaxScore = 0;

    function updateState() {
      const scores = generation.map((i) => i.score);
      let running = 0;

      for (const instance of generation) {
        if (instance.running) running++;
      }

      $(".stat-gen").text(generationI);
      $(".stat-run").text(running + "/" + generation.length);
      $(".stat-max").text(Math.max(...scores));
      $(".stat-min").text(Math.min(...scores));
      $(".stat-lmax").text(lastMaxScore);
    }

    while (shouldTrain) {
      generationI++;
      // Get generation
      lastMaxScore = spawnGeneration();

      const promises = [];
      // For each network define loop
      for (const instance of generation) {
        promises.push(
          new Promise(async (resolve) => {
            // Game loop
            const game = instance.game.game;
            let closestToFood = gameSize ** 2;
            instance.running = true;
            updateState();
            instance.game.target.on("died won", () => {
              instance.running = false;
              updateState();
              resolve();
            });

            /**
             * move - description
             *
             * @return {type}  description
             */
            function move(data) {
              const { result, activations, input } = data;
              // Apply to game
              let move = [0, 0];
              for (const index in result) {
                const activation = result[index];
                if (activation > move[0]) {
                  move = [activation, index];
                }
              }

              const foodDistance =
                (Math.sqrt((game.head[0] - game.food[0]) ** 2, (game.head[1] - game.food[1]) ** 2) - 1) / gameSize;

              if (instance.best) updateRenderedNetwork(input, activations, move[1]);

              if (foodDistance < closestToFood) closestToFood = foodDistance;

              instance.score = (game.snake.length - 3)*8 + Math.sqrt(game.moves) - closestToFood**2;

              instance.game.move(move[1], true);
            }

            // Listen to results
            instance.worker.onmessage = function (ev) {
              const data = ev.data;
              switch (data.type) {
                case "evaluate":
                  move(data.data);
                  break;

                case "network":
                  instance.network = JSON.parse(data.data);
                  // Render neural network of the last best
                  if (instance.best) {
                    $nnDisplay.html(
                      renderNeuralNetwork(instance.network, {
                        width: $nnDisplay.width(),
                        height: $nnDisplay.height(),
                        inputs: Object.keys(getInput(instance.game.game)),
                        outputs: ["front", "right", /*"back",*/ "left"],
                      }),
                    );
                  }
                  break;

                case "boot":
                  if (instance.best) instance.worker.postMessage({ cmd: "getNetwork" });

                  startLoop();
                  break;
              }
            };

            // Run protocol
            instance.worker.postMessage({
              cmd: "boot",
              args: [layers, Object.keys(getInput(displayGame.game)).length, 3, JSON.stringify(instance.network)],
              rate: mutationRate,
            });

            async function startLoop() {
              while (instance.running && shouldTrain) {
                const input = getInput(game);

                // Evaluate input
                instance.worker.postMessage({
                  cmd: "evaluate",
                  input: Object.values(input),
                });

                // Await + delay
                await new Promise(function (resolve) {
                  instance.game.target.one("moved", () => {
                    setTimeout(() => {
                      resolve();
                    }, instance.best * delay );
                  });
                });
              }
            }
          }),
        );
      }

      // Await all instances to die
      await Promise.all(promises);
    }

    training = false;
  }

  /**
   * updateRenderedNetwork - description
   *
   * @param  {type} input       description
   * @param  {type} activations description
   * @param  {type} key         description
   * @return {type}             description
   */
  function updateRenderedNetwork(input, activations, key) {
    const layers = [Object.values(input), ...activations];

    for (let i = layers.length; i-- != 0; ) {
      const layer = layers[i];
      for (let j = layer.length; j-- != 0; ) {
        const $node = $("circle[data-id=node-" + i + "-" + j + "]", $nnDisplay);
        const hex = Math.floor(Math.min(1, Math.max(0, layer[j])) * 255)
          .toString(16)
          .padStart(2, "0");
        $node.attr("fill", "#" + hex.repeat(3));
        $("title", $node).text(Math.floor(layer[j] * 100) / 100);
        $("text[data-id=node-value-" + i + "-" + j + "]", $nnDisplay).text(Math.floor(layer[j] * 100) / 100);
      }
    }

    $("text.node-label-" + (layers.length - 1), $nnDisplay).removeClass("highlight");
    $("text[data-id=node-label-" + (layers.length - 1) + "-" + key + "]", $nnDisplay).addClass("highlight");
  }

  /**
   * getInput - description
   *
   * @param  {type} game description
   * @return {type}      description
   */
  function getInput(game) {
    function getObstacleDistance(direction, position, body, food = false) {
      direction = direction.mod(4);
      const axis = Number(Math.floor(direction + 0.5) == 0 || Math.floor(direction + 0.5) == 2);
      const antiAxis = (axis - 1).mod(2);
      const pos = position[axis];
      const neg = Math.floor(direction + 0.5) == 0 || Math.floor(direction + 0.5) == 3;
      const distances = [];

      if (food) {
        distances.push(
          (() => {
            if (Math.floor(direction) != direction) {
              const rDirection = Math.floor(direction);
              const foodDirection = Math.abs(
                ((Math.PI - Math.atan2(game.food[1] - game.head[1], game.head[0] - game.food[0])) / (2 * Math.PI)) * 4 +
                  1,
              ).mod(4);
              if (foodDirection < rDirection + 1 && foodDirection > rDirection) {
                return Math.sqrt((game.food[0] - game.head[0]) ** 2, (game.food[1] - game.head[1]) ** 2);
              }
            } else if (position[antiAxis] == game.food[antiAxis]) {
              if ((neg && pos > game.food[axis]) || (!neg && pos < game.food[axis])) {
                return Math.abs(pos - game.food[axis]) - 1;
              }
            }
            return gameSize;
          })(),
        );
      } else {
        // Wall distance
        if (neg) distances.push(pos);
        else distances.push(gameSize - pos - 1);
        // Body distance
        for (const segment of body.slice(1)) {
          if (position[antiAxis] != segment[antiAxis]) continue;
          if (neg ? pos < segment[axis] : pos > segment[axis]) continue;
          distances.push(Math.abs(segment[axis] - pos) - 1);
        }
      }

      return (Math.min(...distances) / gameSize)**0.3;
    }

    return {
      // foodDirection: (Math.abs((Math.PI - Math.atan2(
      //   game.food[1] - game.head[1],
      //   game.head[0] - game.food[0]
      // )) / (2*Math.PI) * 4 - game.direction + 1) % 4) / 4,
      // foodDistance: (Math.sqrt(
      //   (game.head[0] - game.food[0])**2,
      //   (game.head[1] - game.food[1])**2
      // ) - 1) / gameSize,
      foodFront: 1 - getObstacleDistance(game.direction, game.head, game.snake, true),
      foodRight: 1 - getObstacleDistance(game.direction + 1, game.head, game.snake, true),
      foodFrontRight: 1 - getObstacleDistance(game.direction + 0.5, game.head, game.snake, true),
      // foodBackRight: 1 - getObstacleDistance(game.direction + 1.5, game.head, game.snake, true),
      foodLeft: 1 - getObstacleDistance(game.direction - 1, game.head, game.snake, true),
      foodFrontLeft: 1 - getObstacleDistance(game.direction - 0.5, game.head, game.snake, true),
      // foodBackLeft: 1 - getObstacleDistance(game.direction - 1.5, game.head, game.snake, true),
      obstacleFront: 1 - getObstacleDistance(game.direction, game.head, game.snake),
      obstacleRight: 1 - getObstacleDistance(game.direction + 1, game.head, game.snake),
      obstacleLeft: 1 - getObstacleDistance(game.direction - 1, game.head, game.snake),
      // bodyLength: game.snake.length / gameSize ** 2,
    };
  }

  /**
   * spawnGeneration - description
   *
   * @return {type}  description
   */
  function spawnGeneration() {
    function getMessiah(group) {
      const messiah = { nInputs: group[0].nInputs, layers: [] };

      for (let layerI = 0; layerI < group[0].network.layers.length; layerI++) {
        messiah.layers.push([]);
        for (let nodeI = 0; nodeI < group[0].network.layers[layerI].length; nodeI++) {
          // Choose node from random and put it into messiah's brain
          messiah.layers[layerI].push(group[Math.floor(Math.random() * group.length)].network.layers[layerI][nodeI]);
        }
      }

      return messiah;
    }

    function spawnInstance(network, game) {
      if (generation.length >= workers.length) {
        // console.log("spawning worker "+(generation.length+1));
        workers.push(new Worker("js/worker.js"));
      }

      const instance = {
        network: network,
        worker: workers[generation.length],
        game: game || new Snake("<div></div>", gameSize),
        score: 0,
        running: false,
        best: false,
      };
      return (index = generation.push(instance) - 1);
    }

    let lastMaxScore = 0;

    // If first run
    if (!generation.length) {
      // Generate new networks
      for (let i = batchSize; i-- > 0; ) {
        spawnInstance();
      }
      // else
    } else {
      // Kill the bad half of networks
      const bests = generation
        .sort((a, b) => b.score - a.score)
        .slice(0, Math.max(1,Math.floor(generation.length * inheritRate)));
      lastMaxScore = bests[0].score;

      // Give all networks a new game and breed them
      generation.length = 0;
      for(let i = 0; i < bests.length; i++){
        spawnInstance(bests[i].network); //getMessiah(bests)
      }
      for (let i = batchSize - generation.length; i-- != 0; ) {
        spawnInstance(bests[0].network);
      }
    }
    // Give the first (best) one the displayed game
    displayGame.reset();
    generation[0].game = displayGame;
    generation[0].best = true;
    return lastMaxScore;
  }

  /**
   * getGene - description
   *
   * @param  {type} network description
   * @return {type}         description
   */
  function getGene(network) {
    return [
      ...network.layers.flatMap((layer) => layer.flatMap((node) => node.weights)),
      ...network.layers.flatMap((layer) => layer.flatMap((node) => node.bias)),
    ];
  }
})();

/*
 *
 * NeuralNetwork:
 *  Inputs:
 *   Food direction; food distance; front, right and left obstacle distance; length of body
 *  Output:
 *   Forward/backward/left/right
 *
 */
