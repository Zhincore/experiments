
/**
 * renderNeuralNetwork - description
 *
 * @param  {type} network description
 * @return {type}         description
 */
function renderNeuralNetwork(network, inOptions) {
  const options = Object.assign({
    width     : 500,
    height    : 250,
    padding   : 16,
    nodeRadius: 10,
    nodeStroke: 4,
    maxStroke : 3,
    inputs    : ["input"],
    outputs   : ["output"]
  }, inOptions);


  /**
   * $SVG - description
   *
   * @param  {type} tag description
   * @return {type}     description
   */
  function $SVG(tag) {
    return $(document.createElementNS("http://www.w3.org/2000/svg", tag));
  }

  const width     = options.width,
        height    = options.height,
        columns   = network.layers.length+2,
        colwidth  = width / columns,
        graNodes  = [],
        graConns  = [];

  const $canvas = ($SVG("svg")
    .attr("width"   , width)
    .attr("height"  , height)
    .attr("viewbox" , "0 0 "+width+" "+height)
  );

  // prepare rendering
  for(let i = 1; i < columns; i++){
    const x = colwidth*i;
    const layer = (i == 1 ? options.inputs : network.layers[i-2]);
    const graIndex = graNodes.push([]) - 1;
    graConns.push([])

    for(let nodeI = 0; nodeI < layer.length; nodeI++){
      const node = layer[nodeI];
      const y = nodeI * (
        options.nodeRadius +
        options.nodeStroke +
        options.padding*2
      ) + options.padding + options.nodeRadius + options.nodeStroke/2;

      // render node
      graNodes[graIndex].push([x,y]);


      if(i == 1) continue;
      const prevGraLayer = graNodes[graIndex-1];
      const connIndex = graConns[graIndex].push([]) - 1;
      for(let prevNodeI = 0; prevNodeI < prevGraLayer.length; prevNodeI++){
        const prevNode = prevGraLayer[prevNodeI];
        const weight = node.weights[prevNodeI];
        graConns[graIndex][connIndex].push([
          "M "+x+","+y+" L "+prevNode[0]+","+prevNode[1],
          weight
        ]);

      }
    }
  }

  // render
  for(let i = graNodes.length; i--!=0;){
    const graLayer = graNodes[i];
    const graLayerConns = graConns[i].flat().sort((a,b)=>Math.abs(b[1]) - Math.abs((a[1])));

    // render connections
    for(let j = graLayerConns.length; j--!=0;){
      const graLine = graLayerConns[j];
      const line = ($SVG("path")
        .attr("d", graLine[0])
        .attr("class", "nn-node-connection")
        .attr("stroke-width", options.maxStroke * Math.abs(graLine[1]))
      );
      line.toggleClass("negative", graLine[1] < 0);
      $canvas.append(line);
    }

    // render nodes
    for(let j = graLayer.length; j--!=0;){
      const graNode = graLayer[j];
      const $node = ($SVG("circle")
        .attr("cx", graNode[0])
        .attr("cy", graNode[1])
        .attr("r" , options.nodeRadius)
        .attr("class", "nn-node node-"+i)
        .attr("data-id", "node-"+i+"-"+j)
        .attr("stroke-width", options.maxStroke)
      );
      $canvas.append($node.append($SVG("title").text("")));

      // render values
      $canvas.append($SVG("text")
        .text("")
        .attr("x", graNode[0])
        .attr("y", graNode[1] + options.nodeRadius + options.nodeStroke/2 + options.padding*0.75)
        .attr("class", "node-value node-value-"+i)
        .attr("data-id", "node-value-"+i+"-"+j)
        .attr("text-anchor", "middle")
        .attr("dominant-baseline", "baseline")
      );

      // render output names
      if(i == graNodes.length-1){
        $canvas.append($SVG("text")
          .text(options.outputs[j])
          .attr("x", graNode[0] + options.nodeRadius + options.nodeStroke/2 + options.padding)
          .attr("y", graNode[1])
          .attr("class", "node-label node-label-"+i)
          .attr("data-id", "node-label-"+i+"-"+j)
          .attr("text-anchor", "start")
          .attr("dominant-baseline", "central")
        );
      }
      // render input names
      else if(i == 0){
        $canvas.append($SVG("text")
          .text(options.inputs[j])
          .attr("x", graNode[0] - options.nodeRadius - options.nodeStroke - options.padding)
          .attr("y", graNode[1])
          .attr("class", "node-label node-label-"+i)
          .attr("text-anchor", "end")
          .attr("dominant-baseline", "central")
        );
      }
    }
  }

  return $canvas.get();
}
