class Snake{
  constructor(target, size){
    this.target = $(target);
    this.viewport = this.target;
    this.size = Math.max(size, 8);

    this.game = {};
    this.reset();
  }

  /**
   * reset - description
   *
   * @return {type}  description
   */
  reset(){
    this.viewport.removeClass("died").removeClass("won");
    this.game = {
      head  : [2, 6],
      snake : [[2,6],[2,5],[2,4],[2,3],[2,2]],
      food  : [7, 7],
      direction: 2,
      moves: 0,
      maxMoves: 0
    }

    this._update();
  }

  /**
   * _update - description
   *
   * @return {type}  description
   */
  _update(render=true){
    // move snake
    if(!matchPos(this.game.head, this.game.snake[0])){
      let lastSeg = this.game.head;
      for(let i = 0; i < this.game.snake.length; i++){
        let seg = this.game.snake[i];
        const prevSeg = [...seg];
        setVec(seg, lastSeg);
        lastSeg = prevSeg;
        // check for dying
        if (!matchPos(seg, [-1,-1]) && (
            seg[0] < 0            || seg[1] < 0           ||
            seg[0] > this.size-1  || seg[1] > this.size-1 )){
          return this._die();
        }
      }
    }

    // check for dying
    const maxMoves = this.game.maxMoves = this.size*this.game.snake.length;
    if(
      this.game.moves >= maxMoves ||
      this.game.snake.some( (c,i)=>(i != 0 && matchPos(this.game.head,c)) )
    ){
      return this._die();
    }

    // check for food
    if(!this.game.food || matchPos(this.game.head, this.game.food)){

      this.game.snake.push([-1,-1]);
      this.target.trigger("ate");
      this.game.moves = 0;

      // check for winning
      if(this.game.snake.length == this.size**2){
        return this._win();
      }

      // reset food
      do{
        setVec(this.game.food, [
          Math.floor(Math.random() * this.size),
          Math.floor(Math.random() * this.size)
        ]);
      } while(this.game.snake.some(c=>matchPos(this.game.food, c)));
      this._update(render);

    }else{
      if(render) this.render();
    }

    // update stats
    $(".snake-moves", this.target).text(
      (maxMoves - this.game.moves) + "/" + maxMoves
    )
  }

  /**
   * _die - description
   *
   * @return {type}  description
   */
  _die(){
    this.viewport.addClass("died");
    this.target.trigger("died");
  }

  /**
   * _win - description
   *
   * @return {type}  description
   */
  _win(){
    this.viewport.addClass("won");
    this.target.trigger("won");
  }

  /**
   * move - description
   *
   * @param  {type} direction description
   * @return {type}           description
   */
  move(direction, relative=false){
    if(isNaN(direction)) {
      direction = {
        up    : 0,
        down  : 2,
        right : 1,
        left  : 3
      }[direction];
    }
    direction = Number(direction);

    if (relative) direction = (this.game.direction + direction) % 4;

    switch (direction) {
      case 0:
        this.game.head[1]--;
        break;
      case 2:
        this.game.head[1]++;
        break;
      case 1:
        this.game.head[0]++;
        break;
      case 3:
        this.game.head[0]--;
        break;
    }

    this.game.direction = direction;
    this.game.moves++;
    this._update();
    this.target.trigger("moved");
  }

  /**
   * render - description
   *
   * @return {type}  description
   */
  async render(){
    const size = Math.min(this.target.width(), this.target.height());

    const update = !!$("#snake-viewport", this.target).length;
    let $game;

    if(!update){
      this.viewport = $game = ($("<div></div>")
        .attr("id", "snake-viewport")
        .width(size)
        .height(size)
      );
      this.target.append($game);
      this.target.append(`<div>Moves remaining: <span class="snake-moves">0/0</span></div>`);
    }else{
      $game = ($("#snake-viewport", this.target)
        .width(size)
        .height(size)
      );
    }


    for(let y=this.size; y-- >0;){
      let $row;
      if(!update){
        $row = ($("<div></div>")
          .attr("class", "snake-row")
        );
        $game.append($row);
      }

      for(let x=this.size; x-- >0;){
        let cell;
        if(!update){
          cell = ($("<div></div>")
            .attr("id", "snake-cell-"+x+"-"+y)
            .attr("class", "snake-cell empty")
          );
        }else{
          cell = $("#snake-cell-"+x+"-"+y, this.target);
          cell.removeClass("food snake head");
        }

        if(this.game.snake.some(()=>matchPos([x,y],this.game.head) )){
          cell.addClass("head");
        }
        else if(matchPos([x,y], this.game.food)){
          cell.addClass("food");
        }
        else if(this.game.snake.some(cell=>matchPos([x,y],cell))){
          cell.addClass("snake");
        }


        if(!update){
          $row.append(cell);
        }
      }
    }
  }
}


function matchPos(v1,v2) {
  v1 = v1 || [null,null]; v2 = v2 || [null,null];
  return v1[0] == v2[0] && v1[1] == v2[1];
}

function setVec(v1,v2) {
  v1 = v1 || [null,null]; v2 = v2 || [null,null];
  v1[0] = v2[0];
  v1[1] = v2[1];
  return v1;
}
