importScripts("NeuralNetwork.js");

let network;
onmessage = async function(ev) {
  const data = ev.data;
  switch(data.cmd){
    case "boot":
      network = new NeuralNetwork(...data.args);
      network.mutate(data.rate);
      postMessage({
        type: "boot"
      });
      break;

    case "evaluate":
      const result = await network.evaluate(data.input);
      postMessage({
        type: "evaluate",
        data: {...result, input:data.input}
      });
      break;

    case "getNetwork":
      postMessage({
        type: "network",
        data: network.export()
      });
      break;
  }
};
