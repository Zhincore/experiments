'use strict';
function Obstacle(canvas, scale, pos, offset, image, hitbox){

  this.render = function(){
    var ctx = canvas.ctx;
    var height = canvas.dom.height;
    var scale = canvas.scale;
    var scaledWidth = Math.floor(image.width * (height / scale));
    var scaledHeight = Math.floor(image.height * (height / scale));

    /// Draw body
    ctx.drawImage(
      image,
      pos.x + offset.x - scaledWidth/2,
      pos.y + offset.y - scaledHeight/2,
      scaledWidth,
      scaledHeight,
    );

    return;
  }
}
