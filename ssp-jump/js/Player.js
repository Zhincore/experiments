'use strict';
function Player(x, y, resources, scale, canvas){
  var size = 96;

  var resources = Object.assign({
    body: null
  }, resources);

  var pos = new SAT.Vector(x,y);
  var origPos = pos.clone();
  // Velocity
  var vel = new SAT.Vector();

  // Controls
  var jumpVector = new SAT.Vector(0, -size*2);
  var canJump = true;
  var canDoubleJump = true;
  var hasJumped = false;

  var hitbox = new SAT.Circle(pos, size/2);


  this.render = function(){
    var ctx = canvas.ctx;
    var width = canvas.dom.width;
    var height = canvas.dom.height;
    var scaledSize = Math.floor(size * (height / scale));

    /// Draw body
    ctx.drawImage(
      resources.body,
      pos.x - scaledSize/2,
      pos.y - scaledSize/2,
      scaledSize,
      scaledSize,
    );

    return;
  }


  this.physics = function(delta, obstacles){
    var height = canvas.dom.height;
    var scaleRatio = height / scale;

    /// Velocity
    // Add gravity
    vel.add(new SAT.Vector(0, 9.8 * 0.4 * scaleRatio * 2));
    // Scale Velocity
    var applyVel = vel.clone().scale(delta/60, delta/60);
    // Apply velocity #
    pos.add(applyVel);
    // Substract already applied velocity
    vel.sub(applyVel);


    /// Update hitbox
    hitbox.r = size * scaleRatio;


    /// Collisions
    obstacles.forEach((obj)=>{
      var collision = new SAT.Response();

      if(SAT.testPolygonCircle(obj, hitbox, collision.clear())){
        vel.add(collision.overlapV);

        // If touched by bottom reset jumping limits
        if(vectorAngle(0,0, collision.overlapN.x,collision.overlapN.y) < 0){
          if(!hasJumped) canJump = canDoubleJump = true;
        }
      }
    });
    hasJumped = false;

    return;
  }

  this.getPos = function(){
    return pos.clone();
  }

  this.jump = function(){
    var height = canvas.dom.height;
    var scaleRatio = height / scale;

    if(canJump){
      vel.add(jumpVector.clone().scale(scaleRatio,scaleRatio));
      canJump = false;
      hasJumped = true;

    }else if (canDoubleJump) {
      // Replace velocity
      vel.copy(jumpVector.clone().scale(scaleRatio,scaleRatio));
      canDoubleJump = false;
      hasJumped = true;
    }
  }
}

/*Player.prototype.physics = function(delta, objects=[], ctx){
  //// Update forces

  /// Add gravitational force
  this.vel.add(new SAT.V(0, this.mass/10 * delta));

  /// Move towards desired x position
  this.vel.add(new SAT.V((this.origPos.clone().sub(this.pos).x/20000)*delta, 0));

  //// Apply forces
  this.pos.add(this.vel);

  /// Loose velocity
  var friction = 0.06 * delta;
  this.vel.scale(friction, friction);

  //// Update colisions
  /// Generate hitbox from last position to new position
  var hitbox = new SAT.Box(
    new SAT.V(
      this.pos.x - this.radius,
      this.pos.y - this.radius
    ),
    this.radius*2,
    this.radius*2
  );

  var collision = new SAT.Response();
  hitbox = this.hitbox = hitbox.toPolygon();

  // Solve collisions and allow jumping
  objects.forEach((obj)=>{
    if(SAT.testPolygonPolygon(obj, hitbox, collision.clear())){
      this.pos.add(collision.overlapV);
      if(!this.hasJumped) this.canJump = this.canDoubleJump = true;
    }
  });
  this.hasJumped = false;
}*/
