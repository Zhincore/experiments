function deg2rad(angle){
  return (angle / 180) * Math.PI;
}

function rad2deg(angle){
  return angle / Math.PI * 180;
}

function vectorAngle(ax, ay, bx, by){
  return Math.atan2(by - ay, bx - ax);
}

function vectorLength(ax, ay, bx, by){
  let a = ax - bx;
  let b = ay - by;

  return Math.sqrt( a*a + b*b );
}

function rotateVector(vec, ang){
    var cos = Math.cos(ang);
    var sin = Math.sin(ang);
    return new Array(Math.round(10000*(vec[0] * cos - vec[1] * sin))/10000, Math.round(10000*(vec[0] * sin + vec[1] * cos))/10000);
}

function deltaFromMs(msdt){
  return Math.min(1, (msdt / 1000) ); /* Limit to 1 second delta to prevent jumping across map in lag spikes*/
}

function ajaxBytes(url, callback=()=>{}){
  let xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.responseType = 'arraybuffer';

  xhr.onload = (e) => {
    callback(xhr.response);
  };

  xhr.send();
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
