(()=>{
  var bestDistance = 0;
  var distanceWent = 0;

  App.onDie = ()=>{
    $("#dieMenu").fadeIn("fast");

    if(FBInstant) {
      FBInstant.postSessionScore(bestDistance);
      FBInstant
        .getLeaderboardAsync('dist0_leaderboard.' + FBInstant.context.getID())
        .then(leaderboard => {
          return leaderboard.setScoreAsync(bestDistance);
        })
        .catch(error => console.error(error))
      ;
    }
  };

  App.onStats = (dist)=>{
    distanceWent = Math.round(dist);
    if(distanceWent > bestDistance) {
      bestDistance = distanceWent;
    }

    $(".stats-dist").text(distanceWent+" pixelů");
  };

  //// Die menu
  $("#menuButton-tryAgain").click(()=>{
    $("#dieMenu").slideUp("fast", ()=>{
      App.startGame();
    });
  });

  $("#menuButton-backToMenu").click(()=>{
    $("#mainMenu").slideDown()
    $("#dieMenu").fadeOut();

    if(FBInstant){
      /*FBInstant.updateAsync({
        action: 'LEADERBOARD',
        name: 'distance_leaderboard.' + FBInstant.context.getID()
      })
      .then(() => console.log('Update Posted'))
      .catch(error => console.error(error));*/
    }
  });


  //// Main menu
  $("#menuButton-play").click(()=>{
    $("#mainMenu").slideUp("fast", ()=>{
      App.startGame();
    });
  });

  $("#menuButton-exit").click(()=>{
    if(FBInstant) FBInstant.quit();
  });


})();
