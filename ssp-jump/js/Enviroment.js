'use strict';
function Enviroment(resources, scale, canvas){
  var pos = new SAT.Vector();
  var obstacles = [];

  var topBottomOffset = 32;

  this.canvas = canvas;
  this.scaling = {
    floor:  256,
    wall:   64,
    ceil:   256
  };
  this.resources = Object.assign({
    floor: null,
    wall: null,
    ceil:  null
  }, resources);


  this.render = function(){
    var ctx = this.canvas.ctx;
    var width = this.canvas.dom.width;
    var height = this.canvas.dom.height;
    var scaleRatio = height / scale;

    /// Render wall
    if(this.resources.wall){
      var wallSize = Math.floor(this.scaling.wall * scaleRatio);
      var wallOffset = Math.floor(-pos.x % wallSize);

      for(var x = -wallOffset; x <= width+wallOffset; x += wallSize){
        var wallHeight = wallSize * (this.resources.wall.height / this.resources.wall.width);

        for(var y = 0; y < height; y += wallHeight){
          ctx.drawImage(
            this.resources.wall,
            -wallOffset + x,
            y,
            wallSize,
            wallHeight
          );
        }
      }
    }

    /// Render floor
    if(this.resources.floor){
      var floorSize = Math.floor(this.scaling.floor * scaleRatio);
      var floorOffset = Math.floor(-pos.x % floorSize);

      for(var x = -floorOffset; x <= width+floorOffset; x += floorSize){
        var floorHeight = floorSize * (this.resources.floor.height / this.resources.floor.width);

        ctx.drawImage(
          this.resources.floor,
          -floorOffset + x,
          height - floorHeight,
          floorSize,
          floorHeight
        );
      }
    }

    /// Render ceiling
    if(this.resources.ceil){
      var ceilSize = Math.floor(this.scaling.ceil * scaleRatio);
      var ceilOffset = Math.floor(-pos.x % ceilSize);

      for(var x = -ceilOffset; x <= width+ceilOffset; x += ceilSize){
        var ceilHeight = ceilSize * (this.resources.ceil.height / this.resources.ceil.width);

        ctx.drawImage(
          this.resources.ceil,
          -ceilOffset + x,
          0,
          ceilSize,
          ceilHeight
        );
      }
    }

    return;
  }


  this.getObstacles = function(){
    var width = this.canvas.dom.width;
    var height = this.canvas.dom.height;
    var scaleRatio = height / scale;
    var output = obstacles.slice(0);

    // TODO: FIX WRONG FLOOR AND CEILINIG HITBOX DISPLAY SCALING

    /// Add floor
    output.push(new SAT.Box(
      new SAT.Vector(
        0,
        height - topBottomOffset * scaleRatio
      ),
      width,
      topBottomOffset * scaleRatio
    ).toPolygon());

    /// Add ceiling
    output.push(new SAT.Box(
      new SAT.Vector(
        0,
        0
      ),
      width,
      topBottomOffset * scaleRatio
    ).toPolygon());

    return output;
  }


  this.translate = function(x){
    var height = this.canvas.dom.height;
    var scaleRatio = height / scale;
    pos.x -= x * scaleRatio;
  }
}
