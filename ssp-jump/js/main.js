'use strict';
var App = {
  //// Config
  options:{
    scale: 720,
    dev:{
      fpsLimit: 0, // Automatic if 0
      drawPhysics: true,
      usePhysics: true,
      physicsDelta: 1/60 *1000 // 60 FPms
    }
  },
  envConfig: {
    theme: {
      floor: "linoleum.png"
    }
  },

  onReady: ()=>{},
  onDie: ()=>{},
  onStats: (distance)=>{},

  //// Internals
  shouldRender: true,
  isReady: false,
  canvas: {
    main: {
      dom: document.getElementById("mainCanvas"),
      ctx: null
    }
  },
  player: null,
  enviroment: null,
  stats: null,
  timing: {
    delta: 2,
    timePlayed: 0,
    gameSpeed: 1/15,
    rendered: false,
    accumulator: 0
  },
  controls: {
    jump: false
  },
  animFrame: null,

  assets: {
    total: 0,
    progress: 0,
    js: [
      "lib/SAT.min.js",
      "lib/stats.min.js",
      "util.js",
      "Player.js",
      "Enviroment.js",
      "Obstacle.js",
      "map/chodbaStructures.js",
    ],
    json: [
      //"structures.json",
    ],
    img: [
      "skin/player/ducky.svg",
      "enviroment/linoleum.png",
      "enviroment/wall_kachla.png",
      "enviroment/dropped_ceiling.png",
      "enviroment/chair.svg",
    ]
  },

  data: {},
  img: {},

  ////////////////
  ///// INIT /////
  ////////////////
  init: async function (progressCB=()=>{}) {
    //// Load assets
    this.assets.total += this.assets.js.length;
    this.assets.total += this.assets.json.length;
    this.assets.total += this.assets.img.length;

    await this.loadScripts(progressCB);
    await this.loadData(progressCB);
    await this.loadImages(progressCB);


    //// Init canvases
    Object.keys(this.canvas).forEach((item)=>{
      this.canvas[item].dom.width = window.innerWidth;
      this.canvas[item].dom.height = window.innerHeight;
      this.canvas[item].ctx = this.canvas[item].dom.getContext("2d");
      this.canvas[item].clear = (function(){
        this.ctx.clearRect(0, 0, this.dom.width, this.dom.height);
      });
    });

    //// Init stats.js
    this.stats = new Stats();
    this.stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
    $("#game").append( $(this.stats.dom).attr("id", "game-stats") );

    //// Register events
    /// KEY DOWN
    $(document).keydown((ev)=>{
      switch(ev.which){
        case 13: // Enter
        case 32: // Spacebar
        case 38: // Arrow up
        case 87: // W
          this.controls.jump = true;
          break;
      }
    });

    /// KEY UP
    $(document).keyup((ev)=>{
      switch(ev.which){
        case 13: // Enter
        case 32: // Spacebar
        case 38: // Arrow up
        case 87: // W
          this.controls.jump = false;
          break;
      }
    });

    /// MOUSE DOWN
    $(document).on("mousedown touchstart", (ev)=>{
      this.controls.jump = true;
    });

    /// MOUSE UP
    $(document).on("mouseup touchend", (ev)=>{
      this.controls.jump = false;
    });

    this.isReady = true;
    this.onReady();
  },

  //// Start new game
  startGame: function(){
    if(!this.isReady) this.onReady = this.startGame;

    /// Stop render loop
    this.shouldRender = false;
    try{
      if(this.animFrame) window.cancelAnimationFrame(this.animFrame);
    }catch(e){}
    this.clearAll();


    /// Reset timers
    this.timing.rendered = false;
    this.timing.timePlayed = 0;

    /// Create a new player
    this.player = new Player(window.innerWidth*0.25, window.innerHeight*0.75-50, {
      body: this.img["ducky.svg"]
    }, this.options.scale, this.canvas.main);

    /// Create a new enviroment
    this.enviroment = new Enviroment({
      floor: this.img["linoleum.png"],
      wall:  this.img["wall_kachla.png"],
      ceil: this.img["dropped_ceiling.png"],
    }, this.options.scale, this.canvas.main);

    /// (re)start render loop
    setTimeout(()=>{
      this.shouldRender = true;
      this.render();
    }, 10);
  },

  //////////////////
  ///// ASSETS /////
  //////////////////
  loadScripts: function(progressCB){
    return new Promise((resolve)=>{
      if(this.assets.js.length == 0) return resolve();

      //// Load scripts
      var loadedFiles = [];

      this.assets.js.forEach((item)=>{
        $.getScript("js/"+item).then((()=>{

          loadedFiles.push(item);
          this.assets.progress++;
          progressCB(this.assets.progress / this.assets.total * 100);

          if(loadedFiles.length == this.assets.js.length) resolve();
        }).bind(this));
      });
    });
  },

  loadData: function(progressCB){
    return new Promise((resolve)=>{
      if(this.assets.json.length == 0) return resolve();

      //// Load scripts
      var loadedFiles = [];

      this.assets.json.forEach((item)=>{
        $.getJSON("data/"+item).then(((data)=>{

          this.data = Object.assign(this.data, data);

          loadedFiles.push(item);
          this.assets.progress++;
          progressCB(this.assets.progress / this.assets.total * 100);

          if(loadedFiles.length == this.assets.json.length) resolve();
        }).bind(this));
      });
    });
  },

  loadImages: function(progressCB){
    return new Promise((resolve)=>{
      if(this.assets.img.length == 0) return resolve();

      //// Load scripts
      var loadedFiles = [];

      this.assets.img.forEach((item)=>{
        var img = new Image();
        img.onload = ()=> {
          var name = item.split("/").pop();
          this.img[name] = img;

          img.width = img.naturalWidth || 1024;
          img.height = img.naturalHeight || 1024;

          loadedFiles.push(item);
          this.assets.progress++;
          progressCB(this.assets.progress / this.assets.total * 100);

          if(loadedFiles.length == this.assets.img.length) resolve();
        }
        img.src = 'img/'+item;
      });
    });
  },

  //////////////////
  ///// RENDER /////
  //////////////////
  render: function() {
    this.stats.begin();
    performance.mark("renderStop");
    if(this.timing.rendered){
      performance.measure("renderMeasure", "renderStart", "renderStop");

      var renderMeasure = performance.getEntriesByName("renderMeasure")[0];

      this.timing.delta = renderMeasure.duration;
      this.timing.timePlayed += this.timing.delta;

      performance.clearMarks();
      performance.clearMeasures();

    }else{
      this.timing.rendered = true;
    }
    performance.mark("renderStart");

    //// Clear
    this.canvas.main.clear();


    //// ENVIROMENT
    this.enviroment.render();


    //// PLAYER
    /// Player controls
    if(this.controls.jump) {
      this.player.jump();
      this.controls.jump = false;
    }
    if(this.options.dev.usePhysics){
      this.timing.accumulator += this.timing.delta;

      while(this.timing.accumulator >= this.options.dev.physicsDelta){
          this.player.physics(
            this.options.dev.physicsDelta,
            this.enviroment.getObstacles()
          );
          this.enviroment.translate(this.options.dev.physicsDelta * this.timing.gameSpeed);

          this.timing.accumulator -= this.options.dev.physicsDelta;
      }
    }

    this.player.render();


    //// GAME
    if(this.player.getPos().x < -this.player.radius) { // If fell out of map
      this.shouldRender = false;
      return this.onDie();
    }

    (new Obstacle(this.canvas.main, this.options.scale, new SAT.V(100,50), new SAT.V(0,0), this.img["chair.svg"], null)).render();


    //this.onStats(-this.enviroment.pos.x);

    this.stats.end();
    if(this.shouldRender) {
      if(this.options.dev.fpsLimit){
        setTimeout(this.render.bind(this), 1000/this.options.dev.fpsLimit);
      }else{
        this.animFrame = window.requestAnimationFrame(this.render.bind(this));
      }
    }
  },

  ///////////////////
  ///// UTILITY /////
  ///////////////////
  clearAll: function() {
    Object.keys(this.canvas).forEach((item)=>this.canvas[item].clear());
  }
}
